package Task1_3_Comptador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Main {

	public static void main(String[] args) {
		// wc -w /->/ Para contar palabras
		// wc -m /->/ Para contar caracteres
		
		// Creamos el proceso
		String[] commandIargs = new String[] {"wc", "-w" , "-m"};
		ProcessBuilder pb = new ProcessBuilder(commandIargs);
		
		pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);

		
		try {
			// Definimos el bufer de entrada del teclado
			InputStreamReader isr = new InputStreamReader(System.in,"UTF-8");
			BufferedReader br = new BufferedReader (isr);
			String cadena = null;

			//captura una cadena de teclat
			System.out.println("comptadortxt>");
			
			// Mientras el texto ingresado no sea null y sea diferente de 0
			while((cadena = br.readLine())!=null && cadena.length()!=0){
			
				System.out.println("  Paraules  Lletres\n"+"  ========  =======");
				
				Process  procesFill = pb.start();	// Iniciamos el proceso
				OutputStream os = procesFill.getOutputStream();//Flujo que conecta la entrada del proceso
				
				OutputStreamWriter osr = new OutputStreamWriter(os,"UTF-8");
				osr.write(cadena); // Escribe la cadena a la entrada del proceso
				osr.flush();
				os.close();		// Cierra el flujo
				
				int codiRet = procesFill.waitFor();

				System.out.println(cadena != null ? "comptadortxt>" : "");
				
			}
			
				System.out.println("\nFinalització del programa\n");
			
			
		} catch (IOException e) {
			// TODO: handle exception
		} catch (InterruptedException e) {
			System.err.println("Procés interromput");
			e.printStackTrace();
		}
	}

}