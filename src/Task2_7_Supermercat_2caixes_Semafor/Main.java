package Task2_7_Supermercat_2caixes_Semafor;

public class Main {
		
	public static void main(String[] args) {
		
		System.out.println("["+Thread.currentThread().getName()+"] Programa del supermercat amb semafors i 2 caixes");
		System.out.println("===========================================");
		
		// Creamos objeto Semaforo
		Semafor semafor = new Semafor(2);
		
		Caixa caixa= new Caixa("Caixa1",semafor); // Creamos la caja
		Caixa caixa2 = new Caixa("Caixa2",semafor); // Creamos la caja
		
		Thread client1Thread = new Thread(new Client(caixa,caixa2),"client1");
		Thread client2Thread = new Thread(new Client(caixa,caixa2),"client2");
		Thread client3Thread = new Thread(new Client(caixa,caixa2),"client3");
		Thread client4Thread = new Thread(new Client(caixa,caixa2),"client4");

		
							
		client1Thread.start();
		client2Thread.start();
		client3Thread.start();
		client4Thread.start();
		
		try {
			client1Thread.join();
			client2Thread.join();
			client3Thread.join();
			client4Thread.join();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("["+Thread.currentThread().getName()+"] Finalització fil principal");

		
	}

}
