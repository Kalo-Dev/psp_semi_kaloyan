package Task2_7_Supermercat_2caixes_Semafor;

public class Semafor {
	
	//attr
	private int numRecursosDisponibles;
	//Constructor
	public Semafor(int numAccessos) {
		this.numRecursosDisponibles = numAccessos;
		System.out.println("Creat el SEMAFOR COMPTADOR amb "+ numAccessos + "permesos");
	}
	//------------------------------
	public synchronized void metodeWait() {
		while(numRecursosDisponibles == 0) { //mentre NO condicio,bloquejat fil executor
			try {
				System.out.println("["+Thread.currentThread().getName()
						+"] La caixa NO està disponible , dorm zZzZz...");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}//while
		numRecursosDisponibles--; //canvia condicio, bloquejat fil executor
	}//metodeWait()
	//------------------------------
	
	public synchronized void metodeWakeup() {
		numRecursosDisponibles++; //canvia condicio, desbloquejat fil executor
		System.out.println("["+Thread.currentThread().getName()
				+"] La caixa ja està disponible, desperta fils...");
		notify(); // o notifyAll()
	}//metodeWakeup()
}//class