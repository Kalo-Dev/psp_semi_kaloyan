package Task2_7_Supermercat_2caixes_Semafor;

public class Caixa {

	//atrr
		private String name;
		private boolean isAvailable;
		private final Semafor semafor;
		
		//Constructor
		public Caixa(Semafor semafor) {
			isAvailable = true;
			this.semafor = semafor;
		}
		
		public Caixa(String name, Semafor semafor) {
			isAvailable = true;
			this.name = name;
			this.semafor = semafor;
			System.out.println("Oberta la caixa <"+name+">");
		}
		
		public boolean findCaixa() {
			semafor.metodeWait();
			return isAvailable;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean isAvailable() {
			return isAvailable;
		}
		public void setAvailable(boolean isAvailable) {
			this.isAvailable = isAvailable;
		}
		
		public void agafaCompra() {
			setAvailable(false);
			System.out.println("<"+ name + "> Llegint la compra del [" + Thread.currentThread().getName() +"]");
			for (int i = 0; i < 100000; i++) {
				
			}
		}
		
	

		public void cobramentCompra(String price) {
			System.out.println("<"+ name + "> Import de la compra de [" + Thread.currentThread().getName() +"] " +
					"és de " + price +"€");
		}
		
		public void ticketCompra() {
			System.out.println("<"+ name + "> Donant ticket de compra a [" + Thread.currentThread().getName() +"]");
			setAvailable(true);
			semafor.metodeWakeup();
		}				

}
