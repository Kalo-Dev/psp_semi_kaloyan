package Task2_5_Supermercat_1caixa_waitNotify;

import java.text.DecimalFormat;

public class Client implements Runnable{
	
	private Caixa caixa;
	String threadName="";

	public Client(Caixa caixa) {
		super();
		this.caixa = caixa;
	}

	@Override
	public void run() { // Se ejecuta todo el código de los hilos
		
		Float randomNum = 5f + (float) (Math.random() * ( 10 - 5 )); // Creo el precio que va a tener la compra del cliente
		DecimalFormat priceFormat = new DecimalFormat("#.00");
		String price = priceFormat.format(randomNum);
		
		threadName = Thread.currentThread().getName();
		
			clientTakeProducts();
			clientGoToQueue();
			caixa.metodeWait();
			caixa.cobramentCompra(price);
			clientPay(price);
			caixa.changeAvailability();
			clientTakePurchase();
			clientGoOut();

		
	}
	
	public void clientTakeProducts() {
		System.out.println("[" + threadName+ "] El client ha fet la recollida de productes");
	}
	
	public void clientGoToTill() {
		System.out.println("["+threadName+"] Client va a la caixa <" + this.caixa.getName() + ">");
	}
	
	public void clientGoToQueue() {
		System.out.println("["+threadName+"] Encuant-se amb la compra a <"+ this.caixa.getName() + ">");
	}
	
	public void clientPay(String price) {
		System.out.println("["+threadName+"] Pagant la compra de " + price +"€ a <" + this.caixa.getName() + ">");
	}
	
	public void clientTakePurchase() {
		System.out.println("["+threadName+"] Recollint la compra a <" + this.caixa.getName() + ">");

	}
	
	public void clientGoOut() {
		System.out.println("[" + threadName+ "] Eixint del supermercat");

	}
	
	
	

	
}
