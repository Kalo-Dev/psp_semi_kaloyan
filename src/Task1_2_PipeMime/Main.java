package Task1_2_PipeMime;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		// Creamos un array de Constructores de Procesos, en el cual indicamos los dos comandos deseados a realizar
		ProcessBuilder[] builders = { new ProcessBuilder("grep","dhcp4"),
											new ProcessBuilder("tail")};
		// Establecemos en un tipo File , la ruta sobre la que se desea ejecutar los comandos anteriores.
		// Se va a utilizar mas adelante como metodo de entrada para el primer proceso.
		File ruta = new File("/var/log/syslog");
		
		try {
			
			// Asignamos al primer proceso la entrada. Le asignamos el directorio creado antes de tipo File
			builders[0].redirectInput(ruta);
			// Asignamos la salida del segundo proceso.
			builders[1].redirectOutput(ProcessBuilder.Redirect.INHERIT); 
			
			
			// Iniciamos los procesos y los enlazamos
			List<Process> listProcess = ProcessBuilder.startPipeline( 
					Arrays.asList(builders));
			
			// Obtenemos el ultimo proceso ejecutado
			Process lastProcess = listProcess.get(listProcess.size()-1);
			
			// Esperamos a que finalice el ultimo proceso.
			lastProcess.waitFor();
			
		} catch (Exception e) {System.err.println(e);}
		 
	}

}
