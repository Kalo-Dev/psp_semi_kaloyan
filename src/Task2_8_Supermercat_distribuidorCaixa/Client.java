package Task2_8_Supermercat_distribuidorCaixa;


import java.text.DecimalFormat;

public class Client implements Runnable{
	
	private Distribuidor distributor;
	private Caixa caixa;
	String threadName="";

	public Client(Distribuidor distributor) {
		super();
		this.distributor = distributor;
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}

	@Override
	public void run() { // Se ejecuta todo el código de los hilos
		
		Float randomNum = 30f + (float) (Math.random() * ( 45 - 30 )); // Creo el precio que va a tener la compra del cliente
		DecimalFormat priceFormat = new DecimalFormat("#.00");
		String price = priceFormat.format(randomNum);
		
		threadName = Thread.currentThread().getName();
		
			clientGoToQueue();
			// Deuria quidrar al distribuidor per que li assigne una caixa
			this.caixa = distributor.assignaCaixa();
			caixa.agafaCompra();
			clientPay(price);
			caixa.finalitzaCompra();
			distributor.finalitzaCompra();
			clientGoOut();

		
	}
	
	public void clientGoToQueue() {
		System.out.println("["+threadName+"] Client va a cua distribuidor");
	}
	
	public void clientPay(String price) {
		System.out.println("["+threadName+"] Ha fet la compra de productes per un import de " + price +" €");
	}
	
	public void clientGoOut() {
		System.out.println("[" + threadName+ "] Client ix del centre comercial");

	}
	
	
	

	
}
