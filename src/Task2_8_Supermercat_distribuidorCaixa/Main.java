package Task2_8_Supermercat_distribuidorCaixa;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<Thread> fils = new ArrayList<Thread>();
		System.out.println("[main] Programa del distribuidor de caixes");
		System.out.println("===========================================");
		Distribuidor distributor = new Distribuidor(3); // Distribuidor amb numero de caixes
		for (int i=1 ; i < 6 ; i++) {
			fils.add(new Thread(new Client(distributor),"Client"+i));
			// Creamos 5 clientes y los iniciamos
		}
		for (Thread fil : fils) {
			fil.start();
		}
		fils.stream().forEach(f -> {
			try {
				f.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		System.out.println("[main] Finalitza fil ppal");


	}

}
