package Task2_8_Supermercat_distribuidorCaixa;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Distribuidor {

	private ArrayList<Caixa> caixes = new ArrayList<Caixa>();
	
	private Lock bloqueig;
	private boolean caixaLliure;
	private Condition clientCondition;

	public Distribuidor(int numCaixes) {
		super();
		bloqueig = new ReentrantLock();
		clientCondition = bloqueig.newCondition();
		caixaLliure = true;

		for (int i = 0; i < numCaixes; i++) {
			caixes.add(new Caixa("Caixa" + (i + 1)));
		}
	}

	// 	public synchronized Caixa assignaCaixa() {
	public Caixa assignaCaixa() {
		bloqueig.lock(); // wait();
		Caixa caixaDisponible = null;
		try {

			if (!caixaLliure) {
				System.out.println("<DISTRIBUIDOR> Cap caixa está disponible, " + "dorm ["
						+ Thread.currentThread().getName() + "] zZzZz...");
				clientCondition.await();
			}
			
			for (Caixa caixa : caixes) {
				if (caixa.isAvailable()) {
					caixaDisponible = caixa;
					caixa.setIsAvailable(false);
					break;
				}
			}
			caixaLliure = caixes.stream().anyMatch(c -> c.isAvailable());
			
			for (int i = 0; i < 1000000; i++) {}
			System.out.println("<DISTRIBUIDOR> Assigna [" + Thread.currentThread().getName() + "] a <"
					+ caixaDisponible.getName() + ">");
			
			//clientCondition.signal();
			bloqueig.unlock();
		} catch (InterruptedException e) {
			System.out.println("ERROR MANGANTE - "+e);
		}
		return caixaDisponible;
	}
	
	// public synchronized void finalitzaCompra()
	public void finalitzaCompra() {
		bloqueig.lock();
		clientCondition.signal();
		bloqueig.unlock();
		// Solo notify();
	}

}
