package Task1_1_ProcessEnvironment;

import java.util.ArrayList;
import java.util.Map;

public class Main {
	
	public static void main(String[] args) {
	
		// Creamos un proceso.
		ProcessBuilder pb = new ProcessBuilder();
		
		// Creamos el entorno del proceso creado anteriormente.
		Map<String,String> env = pb.environment();
				
		// Recorremos el entorno con una expresion lambda e imprimimos el valor en cada iteracion.
		env.forEach((k,v)-> System.out.println("Variable: " + k + 
				"\tValor: " + v + "\n"));
		}
	
}
