package Task2_6_Supermercat_1caixa_lockCondition;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;



public class Caixa {

	//atrr
		private String name;
		private boolean isAvailable;
		private Lock cadenat;
		private Condition disponible;
		
		//Constructor
		public Caixa() {
			isAvailable = true;
		}
		
		public Caixa(String name) {
			isAvailable = true;
			this.name = name;
			cadenat = new ReentrantLock();
			disponible = cadenat.newCondition();
			System.out.println("Oberta la caixa <"+name+">");
		}	

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean isAvailable() {
			return isAvailable;
		}
		
		public void agafaCompra() {
			for (int i = 0; i < 100000; i++) {
				
			}
			this.isAvailable = false;
			System.out.println("<"+ name + "> Llegint la compra del [" + Thread.currentThread().getName() +"]");
		}
		
		public void cobramentCompra(String price) {
			System.out.println("<"+ name + "> Import de la compra de [" + Thread.currentThread().getName() +"] " +
					"és de " + price +"€");
		}
		
		public void ticketCompra() {
			System.out.println("<"+ name + "> Donant ticket de compra a [" + Thread.currentThread().getName() +"]");
			this.isAvailable = true;
		}
		
		public synchronized void metodeWait() {
			try {
				cadenat.lock();
				while(!isAvailable()) {
					System.out.println("["+Thread.currentThread().getName()
							+"] La caixa NO està disponible , dorm zZzZz...");
					disponible.await();
				}//while
				agafaCompra();
				cadenat.unlock();
			} catch (InterruptedException e) {
			e.printStackTrace();
			}
		}//metodeWait
		
		//---------------------------------------
		public synchronized void changeAvailability() {
			cadenat.lock();
			ticketCompra();
			System.out.println("["+Thread.currentThread().getName()
					+"] La caixa ja està disponible, desperta fils...");
			disponible.signalAll();
			cadenat.unlock();
		}//metodeCanviaEstatClau
					

}
