package Task2_6_Supermercat_1caixa_lockCondition;

public class Main {
		
	public static void main(String[] args) {
		
		System.out.println("["+Thread.currentThread().getName()+"] Programa del supermercat amb 1 caixa");
		System.out.println("===========================================");
		
		Caixa caixa = new Caixa("Caixa1"); // Creamos la caja
		
		Thread client1Thread = new Thread(new Client(caixa),"client1");
		Thread client2Thread = new Thread(new Client(caixa),"client2");
		Thread client3Thread = new Thread(new Client(caixa),"client3");
		
							
		client1Thread.start();
		client2Thread.start();
		client3Thread.start();
		
		try {
			client1Thread.join();
			client2Thread.join();
			client3Thread.join();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("["+Thread.currentThread().getName()+"] Finalització fil principal");

		
	}

}
